package task.root.updatedweather.utils

import task.root.updatedweather.R

@Suppress("IMPLICIT_CAST_TO_ANY")
fun getIcon(content: String) = when(content) {
    "Clear" -> R.drawable.ic_sun
    "Rain" -> R.drawable.ic_rain
    "Clouds" -> R.drawable.ic_cloud
    else -> 0
}