package task.root.updatedweather.pojo.hourly

import com.google.gson.annotations.SerializedName

data class CloudsPOJO(
    @SerializedName("all") val all: Int
){
    override fun toString(): String {
        return "All - $all"
    }
}