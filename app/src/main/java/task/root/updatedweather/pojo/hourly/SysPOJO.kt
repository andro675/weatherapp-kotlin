package task.root.updatedweather.pojo.hourly

import com.google.gson.annotations.SerializedName

data class SysPOJO(
    @SerializedName("pod") val pod: String
) {
    override fun toString(): String {
        return "Pod - $pod"
    }
}