package task.root.updatedweather.pojo.hourly

import com.crocusoft.pojo.WeatherPropertiesPOJO
import com.google.gson.annotations.SerializedName

data class HourlyForecastPOJO(
    @SerializedName("dt") val dt: Long,
    @SerializedName("main") val main: MainPOJO,
    @SerializedName("weather") val weather: List<WeatherPropertiesPOJO>,
    @SerializedName("clouds") val clouds: CloudsPOJO,
    @SerializedName("wind") val wind: WindPOJO,
    @SerializedName("sys") val sys: SysPOJO,
    @SerializedName("dt_txt") val date: String
) {
    override fun toString(): String {
        return "MainPOJO - $main\n\n" +
                "Weather - ${weather[0]}\n\n" +
                "CloudsPOJO - $clouds\n\n" +
                "WindPOJO - $wind\n\n" +
                "SysPOJO - $sys\n\n" +
                date
    }
}