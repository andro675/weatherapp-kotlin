package task.root.updatedweather.pojo.hourly

import com.google.gson.annotations.SerializedName
import task.root.updatedweather.pojo.hourly.HourlyForecastPOJO

data class HourlyMainResponsePOJO(
    @SerializedName("cod") val code: String,
    @SerializedName("message") val message: Double,
    @SerializedName("cnt") val cnt: Int,
    @SerializedName("list") val hours: List<HourlyForecastPOJO>
)