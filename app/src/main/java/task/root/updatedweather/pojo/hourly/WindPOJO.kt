package task.root.updatedweather.pojo.hourly

import com.google.gson.annotations.SerializedName

data class WindPOJO(
    @SerializedName("speed") val speed: Double,
    @SerializedName("deg") val degree: Double
) {
    override fun toString(): String {
        return "Speed - $speed\n\n" +
               "Degree - $degree"
    }
}