package task.root.updatedweather.repositories

import task.root.updatedweather.network.API
import task.root.updatedweather.pojo.daily.DailyMainResponsePOJO
import task.root.updatedweather.pojo.hourly.HourlyMainResponsePOJO
import task.root.updatedweather.utils.API_KEY
import task.root.updatedweather.utils.COUNTRY_LAT
import task.root.updatedweather.utils.COUNTRY_LON
import task.root.updatedweather.utils.DAY_COUNT

object FragmentForecastRepository {

    suspend fun getDailyForecast(): DailyMainResponsePOJO =
        API.apiService.getDailyForecast(COUNTRY_LAT, COUNTRY_LON, DAY_COUNT, API_KEY)

    suspend fun getHourlyForecast(): HourlyMainResponsePOJO =
        API.apiService.getHourlyForecast(COUNTRY_LAT, COUNTRY_LON, API_KEY)

}