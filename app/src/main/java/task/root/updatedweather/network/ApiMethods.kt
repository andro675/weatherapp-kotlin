package task.root.updatedweather.network

import retrofit2.http.GET
import retrofit2.http.Query
import task.root.updatedweather.pojo.daily.DailyMainResponsePOJO
import task.root.updatedweather.pojo.hourly.HourlyMainResponsePOJO

interface ApiMethods {

    @GET("daily")
    suspend fun getDailyForecast(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("cnt") count: Int,
        @Query("APPID") appKey: String
    ): DailyMainResponsePOJO

    @GET("hourly")
    suspend fun getHourlyForecast(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("APPID") appKey: String
    ): HourlyMainResponsePOJO

}