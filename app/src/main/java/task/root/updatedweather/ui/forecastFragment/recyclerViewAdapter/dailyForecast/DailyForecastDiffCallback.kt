package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.dailyForecast

import androidx.recyclerview.widget.DiffUtil
import task.root.updatedweather.pojo.daily.DailyWeatherInfoPOJO

class DailyForecastDiffCallback : DiffUtil.ItemCallback<DailyWeatherInfoPOJO>() {

    override fun areItemsTheSame(
        oldItem: DailyWeatherInfoPOJO,
        newItem: DailyWeatherInfoPOJO
    ): Boolean = oldItem.dt == newItem.dt

    override fun areContentsTheSame(
        oldItem: DailyWeatherInfoPOJO,
        newItem: DailyWeatherInfoPOJO
    ): Boolean = oldItem == newItem

}