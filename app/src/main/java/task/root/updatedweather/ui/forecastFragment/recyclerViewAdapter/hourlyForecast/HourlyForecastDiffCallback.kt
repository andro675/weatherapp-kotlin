package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.hourlyForecast

import androidx.recyclerview.widget.DiffUtil
import task.root.updatedweather.pojo.hourly.HourlyForecastPOJO

class HourlyForecastDiffCallback : DiffUtil.ItemCallback<HourlyForecastPOJO>() {

    override fun areItemsTheSame(
        oldItem: HourlyForecastPOJO,
        newItem: HourlyForecastPOJO
    ): Boolean = oldItem.dt == newItem.dt

    override fun areContentsTheSame(
        oldItem: HourlyForecastPOJO,
        newItem: HourlyForecastPOJO
    ): Boolean = oldItem == newItem

}