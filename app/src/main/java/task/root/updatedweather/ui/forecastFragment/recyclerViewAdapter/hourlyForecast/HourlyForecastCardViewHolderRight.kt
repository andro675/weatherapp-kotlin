package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.hourlyForecast

import task.root.updatedweather.databinding.ItemForecastCardRightBinding
import task.root.updatedweather.pojo.hourly.HourlyForecastPOJO
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.BaseViewHolder
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.OnForecastCardClick
import task.root.updatedweather.utils.getIcon

class HourlyForecastCardViewHolderRight(
    private val binding: ItemForecastCardRightBinding,
    private val clickListener: OnForecastCardClick
) : BaseViewHolder(binding.root) {

    fun bind(data: HourlyForecastPOJO) {
        binding.apply {
            mainDescription = "${data.weather[0].main} - ${data.date}"
            imageViewIconForecastCard.setImageResource(getIcon(data.weather[0].main))
            forecastCard.setOnClickListener { clickListener.onForecastClick(data.toString()) }
        }
    }

}