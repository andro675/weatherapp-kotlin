package task.root.updatedweather.ui.catalogFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_catalog.*
import task.root.updatedweather.R
import task.root.updatedweather.ui.forecastFragment.ForecastFragmentArgs

class CatalogFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_catalog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val actionDaily = CatalogFragmentDirections.actionCatalogFragmentToForecastFragment(0)
        val actionHourly = CatalogFragmentDirections.actionCatalogFragmentToForecastFragment(1)

        card_view_daily_forecast.setOnClickListener { findNavController().navigate(actionDaily) }
        card_view_hourly_forecast.setOnClickListener { findNavController().navigate(actionHourly) }
    }

}
