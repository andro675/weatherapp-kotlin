package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.hourlyForecast

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import task.root.updatedweather.R
import task.root.updatedweather.databinding.ItemForecastCardLeftBinding
import task.root.updatedweather.databinding.ItemForecastCardRightBinding
import task.root.updatedweather.pojo.hourly.HourlyForecastPOJO
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.BaseViewHolder
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.OnForecastCardClick
import task.root.updatedweather.utils.inflate

class HourlyForecastAdapter(
    private val clickListener: OnForecastCardClick
) :
    ListAdapter<HourlyForecastPOJO, BaseViewHolder>(HourlyForecastDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder = when (viewType) {
        0 -> HourlyForecastCardViewHolderRight(
            ItemForecastCardRightBinding.bind(parent.inflate(R.layout.item_forecast_card_right)),
            clickListener
        )
        else -> HourlyForecastCardViewHolderLeft(
            ItemForecastCardLeftBinding.bind(parent.inflate(R.layout.item_forecast_card_left)),
            clickListener
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = when (position % 2) {
        0 -> (holder as HourlyForecastCardViewHolderRight).bind(getItem(position))
        else -> (holder as HourlyForecastCardViewHolderLeft).bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int = when (position % 2) {
        0 -> 0
        else -> 1
    }

}