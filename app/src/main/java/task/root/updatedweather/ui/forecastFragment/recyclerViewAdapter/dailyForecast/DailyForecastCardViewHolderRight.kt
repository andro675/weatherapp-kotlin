package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.dailyForecast

import task.root.updatedweather.databinding.ItemForecastCardRightBinding
import task.root.updatedweather.pojo.daily.DailyWeatherInfoPOJO
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.BaseViewHolder
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.OnForecastCardClick
import task.root.updatedweather.utils.getIcon

class DailyForecastCardViewHolderRight(
    private val rootLayout: ItemForecastCardRightBinding,
    private val clickListener: OnForecastCardClick
) : BaseViewHolder(rootLayout.root) {

    fun bind(data: DailyWeatherInfoPOJO) = rootLayout.apply {
        mainDescription = data.weather[0].description
        imageViewIconForecastCard.setImageResource(getIcon(data.weather[0].main))
        forecastCard.setOnClickListener { clickListener.onForecastClick(data.toString()) }
    }

}