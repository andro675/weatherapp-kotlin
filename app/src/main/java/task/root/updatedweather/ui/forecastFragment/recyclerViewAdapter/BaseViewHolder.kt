package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class BaseViewHolder(root: View): RecyclerView.ViewHolder(root)