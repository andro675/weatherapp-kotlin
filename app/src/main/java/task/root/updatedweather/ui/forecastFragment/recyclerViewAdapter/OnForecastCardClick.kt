package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter

class OnForecastCardClick(private val clickListener: (data: String) -> Unit) {

    fun onForecastClick(data: String) = clickListener(data)

}