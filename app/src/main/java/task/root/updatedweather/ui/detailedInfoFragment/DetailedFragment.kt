package task.root.updatedweather.ui.detailedInfoFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs

import task.root.updatedweather.R
import task.root.updatedweather.databinding.FragmentDetailedBinding

class DetailedFragment : Fragment() {

    private val args: DetailedFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentDetailedBinding.inflate(inflater, container, false)
        binding.allInfo = args.allData
        return binding.root
    }

}
