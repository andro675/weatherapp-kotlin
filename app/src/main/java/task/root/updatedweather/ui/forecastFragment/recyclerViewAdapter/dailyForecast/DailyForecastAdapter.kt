package task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.dailyForecast

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import task.root.updatedweather.R
import task.root.updatedweather.databinding.ItemForecastCardLeftBinding
import task.root.updatedweather.databinding.ItemForecastCardRightBinding
import task.root.updatedweather.pojo.daily.DailyWeatherInfoPOJO
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.BaseViewHolder
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.OnForecastCardClick
import task.root.updatedweather.utils.inflate

class DailyForecastAdapter(private val clickListener: OnForecastCardClick) :
    ListAdapter<DailyWeatherInfoPOJO, BaseViewHolder>(DailyForecastDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder =
        when (viewType) {
            0 -> DailyForecastCardViewHolderRight(
                ItemForecastCardRightBinding.bind(parent.inflate(R.layout.item_forecast_card_right)),
                clickListener
            )
            else -> DailyForecastCardViewHolderLeft(
                ItemForecastCardLeftBinding.bind(parent.inflate(R.layout.item_forecast_card_left)),
                clickListener
            )
        }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (position % 2) {
            0 -> (holder as DailyForecastCardViewHolderRight).bind(getItem(position))
            else -> (holder as DailyForecastCardViewHolderLeft).bind(getItem(position))
        }
    }

    override fun getItemViewType(position: Int): Int = when (position % 2) {
        0 -> 0
        else -> 1
    }

}