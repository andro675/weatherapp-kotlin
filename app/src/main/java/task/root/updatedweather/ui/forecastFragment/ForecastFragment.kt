package task.root.updatedweather.ui.forecastFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_forecast.*

import task.root.updatedweather.R
import task.root.updatedweather.pojo.daily.DailyMainResponsePOJO
import task.root.updatedweather.pojo.hourly.HourlyMainResponsePOJO
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.OnForecastCardClick
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.dailyForecast.DailyForecastAdapter
import task.root.updatedweather.ui.forecastFragment.recyclerViewAdapter.hourlyForecast.HourlyForecastAdapter
import task.root.updatedweather.viewModels.FragmentForecastViewModel

class ForecastFragment : Fragment() {

    private val args: ForecastFragmentArgs by navArgs()

    private val viewModel: FragmentForecastViewModel by lazy {
        ViewModelProvider(this).get(
            FragmentForecastViewModel::class.java
        )
    }

    private val navController: NavController by lazy { findNavController() }

    private val onForecastClickListener: OnForecastCardClick = OnForecastCardClick {
        val action = ForecastFragmentDirections.actionForecastFragmentToDetailedFragment(it)
        navController.navigate(action)
    }

    private val dailyForecastAdapter: DailyForecastAdapter by lazy {
        DailyForecastAdapter(
            onForecastClickListener
        )
    }
    private val hourlyForecastAdapter: HourlyForecastAdapter by lazy {
        HourlyForecastAdapter(
            onForecastClickListener
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forecast, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureForecastCardRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        when(args.forecastType) {
            0 -> viewModel.getDailyForecast()
            else -> viewModel.getHourlyForecast()
        }
    }

    private fun configureForecastCardRecyclerView() {
        recycler_view_forecast_cards.adapter = when (args.forecastType) {
            0 -> dailyForecastAdapter
            else -> hourlyForecastAdapter
        }

        viewModel.forecast.observe(this, Observer {
            progress_bar_loading.visibility = GONE
            when (args.forecastType) {
                0 -> dailyForecastAdapter.submitList((it as DailyMainResponsePOJO).days)
                1 -> hourlyForecastAdapter.submitList((it as HourlyMainResponsePOJO).hours)
            }
        })
    }

}
