package task.root.updatedweather.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import task.root.updatedweather.repositories.FragmentForecastRepository

class FragmentForecastViewModel : ViewModel() {

    private val _forecast: MutableLiveData<Any> = MutableLiveData()
    var forecast: LiveData<Any> = _forecast

    fun getDailyForecast() {
        CoroutineScope(IO).launch {
            val response = FragmentForecastRepository.getDailyForecast()
            withContext(Main) {
                _forecast.value = response
            }
        }
    }

    fun getHourlyForecast() {
        CoroutineScope(IO).launch {
            val response = FragmentForecastRepository.getHourlyForecast()
            withContext(Main) {
                _forecast.value = response
            }
        }
    }

}